use std::io;
use std::cmp::Ordering;
use rand::Rng;
use colored::Colorize;
use chrono::prelude::*;

fn get_unix_timestamp_ms() -> i64 {
    let now = Utc::now();
    now.timestamp_millis()
}

fn main() {
    let title = "Adivina el número".purple().bold();
    let decorator = "=".repeat(title.len() - 1).purple().bold();

    println!("\n{}", decorator);
    println!("{}", title);
    println!("{} \n", decorator);

    let secret_number = rand::thread_rng().gen_range(1, 101);

    let mut attempts = 0;

    let start_time: i64 = get_unix_timestamp_ms();

    loop {
        attempts+=1;
        println!("{}", "Ingresa el número secreto".italic().truecolor(180,180,180));
        let mut guess = String::new();

        io::stdin().read_line(&mut guess)
            .expect("Error al leer la entrada");

        let guess: u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => {
                println!("{}", "Debe ser un número válido!".red().italic());
                continue
            },
        };
        
        match guess.cmp(&secret_number) {
            Ordering::Greater => println!("{}\n", "Demasiado alto!".yellow().italic()),
            Ordering::Less => println!("{}\n", "Demasiado bajo!".cyan().italic()),
            Ordering::Equal => {
                let time_passed = get_unix_timestamp_ms() - start_time;
                println!("\n🏆 {} es el número secreto!\n", format!("{}", guess).green().bold());
                println!("{}", "Ganaste, felicidades!".green().italic());
                println!("Lo intentaste: {} veces", format!("{}", attempts).bold());
                println!("Tardaste {} milisegundos\n", format!("{}", time_passed).bold());
                break;
            }
        }
    }
}
